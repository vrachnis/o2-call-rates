#!/usr/bin/env python

from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.keys import Keys


class HTMLChangedException(Exception):
    pass


class InvalidCountryException(Exception):
    pass


class O2Crawler:
    o2_page = 'http://international.o2.co.uk/internationaltariffs/calling_abroad_from_uk'

    def __init__(self):
        self.driver = webdriver.Firefox()

        self.driver.get(self.o2_page)
        self.search_element = None

    def _find_search(self):
        """Find the search box"""

        try:
            self.search_element = self.driver.find_element_by_id(
                'countryName'
            )
        except NoSuchElementException:
            raise HTMLChangedException

    def country_rate(self, country_name, type_of_call='Landline'):
        """
        Find the rates for a country

        Arguments:
          country_name: the name of the country
          type_of_call: (optional) one of 'Landline', 'Mobiles' or
                        'Cost per text message'. By default, 'Landline'
                        is used.
        """

        # Populate the search_element variable only once.
        # Since we will never reload the page we only need
        # to find the search element once, and then reuse it.
        if self.search_element is None:
            self._find_search()

        self.search_element.send_keys(country_name.capitalize())
        self.search_element.send_keys(Keys.ENTER)

        # Wait until the request to the server has finished,
        # and the results have been drawn on the page
        WebDriverWait(self.driver, 10).until(
            expected_conditions.presence_of_element_located(
                (By.ID, 'paymonthly')
            )
        )

        # The search element belongs to the class error when
        # the requested country is invalid
        if 'error' in self.search_element.get_attribute('class'):
            raise InvalidCountryException

        # Find the rate in the standardRatesTable, and return it
        try:
            self.driver.find_element_by_id('paymonthly').click()

            rates = self.driver.find_element_by_id(
                'standardRatesTable'
            ).find_elements_by_xpath('.//tbody/*')
            for rate in rates:
                if type_of_call in rate.text:
                    return rate.text.split()[-1]
        except NoSuchElementException:
            raise HTMLChangedException

    def close(self):
        self.driver.close()


if __name__ == '__main__':
    crawler = O2Crawler()

    for country in [
        'Canada', 'Germany', 'Iceland', 'Pakistan',
        'Singapore', 'South Africa', 'Gallifrey'
    ]:
        try:
            rate = crawler.country_rate(country)
        except HTMLChangedException:
            continue
        except InvalidCountryException:
            rate = 'N/A'
        print u'{}: {} per minute'.format(country, rate)

    crawler.close()
